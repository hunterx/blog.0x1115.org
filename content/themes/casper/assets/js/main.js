var casper = casper || {}
casper.main = {
    onNavClick: function () {
        casper.main.jEs.$navButtons.on("click", function(e){
            e.preventDefault();
            casper.main.jEs.$body.toggleClass("nav-opened nav-closed");
        });
    },

    onDocReady: function () {
        this.jEs.$postContent.fitVids();
        this.jEs.$scrollDown.arcticScroll();
        this.onNavClick.call(this);
    },

    init: function ($) {
        casper.main.jEs.init($);
        casper.main.jPs.init($);
        casper.main.jEs.$document.ready( function () {
            casper.main.onDocReady.call(casper.main);            
        });
    }
};
casper.main.jEs = {
    init: function ($) {
        this.$document = $(document);
        this.$body = $("body");
        this.$postContent = $(".post-content");
        this.$scrollDown = $(".scroll-down");
        this.$navButtons = $(".menu-button, .nav-cover, .nav-close");
    }
};

casper.main.jPs = {
    arcticScroll: function ($) {
        $.fn.arcticScroll = function (options) {

            var defaults = {
                elem: $(this),
                speed: 500
            },

            allOptions = $.extend(defaults, options);

            allOptions.elem.click(function (event) {
                event.preventDefault();
                var $this = $(this),
                    $htmlBody = $('html, body'),
                    offset = ($this.attr('data-offset')) ? $this.attr('data-offset') : false,
                    position = ($this.attr('data-position')) ? $this.attr('data-position') : false,
                    toMove;

                if (offset) {
                    toMove = parseInt(offset);
                    $htmlBody.stop(true, false).animate({scrollTop: ($(this.hash).offset().top + toMove) }, allOptions.speed);
                } else if (position) {
                    toMove = parseInt(position);
                    $htmlBody.stop(true, false).animate({scrollTop: toMove }, allOptions.speed);
                } else {
                    $htmlBody.stop(true, false).animate({scrollTop: ($(this.hash).offset().top) }, allOptions.speed);
                }
            });

        };
    },
    init: function ($) {
        this.arcticScroll($);
    }
};
(function ($, undefined) {
    "use strict";
    casper.main.init($);
})(jQuery);