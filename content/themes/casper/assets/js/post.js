var casper = casper || {}
casper.post = {
    onDocReady: function () {
        casper.post.hljs.call(this);
        casper.post.disqus.init();
    },

    hljs: function () {
        casper.post.jEs.$preCode.each(function(i, block) {
            window.hljs.highlightBlock(block);
        });
    },    

    init: function ($) {
        casper.post.jEs.init($);
        casper.post.jEs.$document.ready( function () {
            casper.post.onDocReady.call(casper.post);
        });
    }
};
casper.post.jEs = {
    init: function ($) {
        this.$document = $(document);
        this.$preCode = $("pre code[class*='language-']");
    }
};
casper.post.disqus = {
    disqus_shortname : '0x1115',
    init : function () {
        window.disqus_shortname = this.disqus_shortname;
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'https://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    }

};

(function ($, undefined) {
    "use strict";
    casper.post.init($);
})(jQuery);